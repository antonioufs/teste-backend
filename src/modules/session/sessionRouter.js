const router = require('express').Router();
const sessionController = require('./sessionController');

router.post('/', sessionController.create);

module.exports = router;
