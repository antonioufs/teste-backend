const authenticateUserService = require('./services/authenticateUserService');

const create = async (req, res) => {
  const { email, password, type } = req.body;

  const { user, token, admin } = await authenticateUserService({
    email,
    password,
    type,
  });

  res.json({ user, admin, token });
};

module.exports = {
  create,
};
