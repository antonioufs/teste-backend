const { sign } = require('jsonwebtoken');
const authConfig = require('../../../config/auth');
const userRepository = require('../../user/userRepository');
const adminRepository = require('../../admin/adminRepository');
const AppError = require('../../../middlewares/AppError');

const AuthenticateUserService = async ({ email, password, type }) => {
  if (type === 'user') {
    const user = await userRepository.findByEmail(email);

    if (!user || user.is_deleted) {
      throw new AppError('Incorrect email or password.');
    }

    const passwordMatched = await user.checkPassword(password);

    if (!passwordMatched) {
      throw new AppError('Incorrect email or password.');
    }

    const { secret, expiresIn } = authConfig.jwt;

    const token = sign(
      {
        id: user.id,
        admin: false,
      },
      secret,
      {
        expiresIn,
      },
    );

    return { user, token };
  }

  const admin = await adminRepository.findByEmail(email);

  if (!admin || admin.is_deleted) {
    throw new AppError('Incorrect email or password.');
  }

  const passwordMatched = await admin.checkPassword(password);

  if (!passwordMatched) {
    throw new AppError('Incorrect email or password.');
  }

  const { secret, expiresIn } = authConfig.jwt;

  const token = sign(
    {
      id: admin.id,
      admin: true,
    },
    secret,
    {
      expiresIn,
    },
  );

  return { admin, token };
};

module.exports = AuthenticateUserService;
