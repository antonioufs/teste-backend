const router = require('express').Router();
const userRouter = require('./user/userRouter');
const adminRouter = require('./admin/adminRouter');
const sessionRouter = require('./session/sessionRouter');
const movieRouter = require('./movie/movieRouter');

router.use('/user', userRouter);
router.use('/admin', adminRouter);
router.use('/session', sessionRouter);
router.use('/movie', movieRouter);

module.exports = router;
