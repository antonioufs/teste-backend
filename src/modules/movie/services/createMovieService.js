const movieRepository = require('../movieRepository');
const actorRepository = require('../../actor/actorRepository');
const AppError = require('../../../middlewares/AppError');

const createMovieService = async (data, admin) => {
  const movieExist = await movieRepository.findByName(data.name);

  if (!admin) {
    throw new AppError('Only administrators can register new movies');
  }

  if (movieExist) {
    throw new AppError('Movie  already registered');
  }

  const actorsExits = await actorRepository.list(data.actors);

  const actorsExitsNames = actorsExits.map(actor => actor.name);

  const actorsNotExits = data.actors
    .filter(actor => actorsExitsNames.indexOf(actor) === -1)
    .map(actor => {
      return { name: actor };
    });

  await actorRepository.create(actorsNotExits);

  const actors = await actorRepository.list(data.actors);

  delete data.actors;

  const movie = await movieRepository.create(data);

  await movie.addActor(actors);

  const newMovie = await movieRepository.findByName(data.name);

  return newMovie;
};

module.exports = createMovieService;
