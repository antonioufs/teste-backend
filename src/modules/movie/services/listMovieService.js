const movieRepository = require('../movieRepository');

const listMovieService = async ({ director, name, genre, actors }) => {
  const movieQuery = {};
  const actorsQuery = {};

  if (director) {
    movieQuery.director = director;
  }
  if (name) {
    movieQuery.name = name;
  }
  if (genre) {
    movieQuery.genre = genre;
  }
  if (actors) {
    actorsQuery.name = actors;
  }

  const movies = await movieRepository.list(movieQuery, actorsQuery);

  return movies;
};

module.exports = listMovieService;
