const movieRepository = require('../movieRepository');
const AppError = require('../../../middlewares/AppError');

const voteMovieService = async (id, rating, admin) => {
  if (admin) {
    throw new AppError('administrators cannot vote');
  }

  if (rating < 0 || rating > 5) {
    throw new AppError('Invalid vote. The vote must be between 0 and 5');
  }

  const movie = await movieRepository.findById(id);

  if (!movie) {
    throw new AppError('Movie not found!', 404);
  }

  if (movie.votes === 0) {
    movie.rating = rating;
  } else {
    movie.rating = (movie.rating * movie.votes + rating) / (movie.votes + 1);
  }
  movie.votes += 1;

  const movieUpdated = await movieRepository.ratingUpdate(movie);

  return movieUpdated;
};

module.exports = voteMovieService;
