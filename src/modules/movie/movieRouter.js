const router = require('express').Router();
const movieController = require('./movieController');
const ensureAuthenticated = require('../../middlewares/ensureAuthenticated');

router.get('/', movieController.list);
router.get('/:id', movieController.show);
router.use(ensureAuthenticated);
router.post('/', movieController.create);
router.patch('/:id', movieController.vote);

module.exports = router;
