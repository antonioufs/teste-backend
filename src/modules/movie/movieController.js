const createMovieService = require('./services/createMovieService');
const listMovieService = require('./services/listMovieService');
const voteMovieService = require('./services/voteMovieService');
const movieRepository = require('./movieRepository');

const create = async (req, res) => {
  const data = req.body;
  const { admin } = req.user;

  const movie = await createMovieService(data, admin);

  return res.json(movie);
};

const list = async (req, res) => {
  const { director, name, genre, actors } = req.query;

  const movies = await listMovieService({ director, name, genre, actors });

  return res.json(movies);
};

const show = async (req, res) => {
  const { id } = req.params;

  const movie = await movieRepository.findById(id);

  return res.json(movie);
};

const vote = async (req, res) => {
  const { id } = req.params;
  const { rating } = req.body;
  const { admin } = req.user;

  const movie = await voteMovieService(id, rating, admin);

  return res.json(movie);
};

module.exports = {
  create,
  list,
  show,
  vote,
};
