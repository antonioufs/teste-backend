const { Movie, Actor } = require('../../models');

module.exports = {
  create: data => Movie.create(data),
  findByName: name =>
    Movie.findOne({ where: { name }, include: [{ model: Actor }] }),
  findById: id =>
    Movie.findByPk(id, {
      include: [
        {
          model: Actor,
          attributes: ['name'],
          through: {
            attributes: [],
          },
        },
      ],
    }),
  list: (movieQuery, actorsQuery) =>
    Movie.findAll({
      where: { ...movieQuery },
      attributes: ['name', 'director', 'genre'],
      include: [
        {
          model: Actor,
          where: { ...actorsQuery },
          attributes: ['name'],
          through: {
            attributes: [],
          },
        },
      ],
    }),
  ratingUpdate: movie => movie.save(),
};
