const adminRepository = require('../adminRepository');
const AppError = require('../../../middlewares/AppError');

const destroyAdminService = async id => {
  const admin = await adminRepository.findById(id);

  if (!admin || admin.is_deleted) {
    throw new AppError('Admin not found', 404);
  }

  admin.is_deleted = true;

  await adminRepository.update(admin);
};

module.exports = destroyAdminService;
