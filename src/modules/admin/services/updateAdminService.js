const adminRepository = require('../adminRepository');
const AppError = require('../../../middlewares/AppError');

const updateAdminService = async (id, data) => {
  const admin = await adminRepository.findById(id);

  if (!admin || admin.is_deleted) {
    throw new AppError('Admin not found', 404);
  }

  if (data.email) {
    const emailExists = await adminRepository.findByEmail(data.email);

    if (emailExists) {
      throw new AppError('Email already registered');
    }
  }

  Object.assign(admin, data);

  const newAdmin = await adminRepository.update(admin);

  return newAdmin;
};

module.exports = updateAdminService;
