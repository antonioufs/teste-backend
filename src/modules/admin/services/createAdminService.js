const adminRepository = require('../adminRepository');
const AppError = require('../../../middlewares/AppError');

const createAdminService = async data => {
  const admin = await adminRepository.findByEmail(data.email);

  if (admin) {
    throw new AppError('Email already registered');
  }

  const newAdmin = await adminRepository.create(data);

  return newAdmin;
};

module.exports = createAdminService;
