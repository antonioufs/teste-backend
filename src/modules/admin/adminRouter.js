const router = require('express').Router();
const adminController = require('./adminController');
const ensureAuthenticated = require('../../middlewares/ensureAuthenticated');

router.post('/', adminController.create);
router.put('/', ensureAuthenticated, adminController.update);
router.delete('/', ensureAuthenticated, adminController.destroy);

module.exports = router;
