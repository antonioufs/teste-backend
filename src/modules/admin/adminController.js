const createAdminService = require('./services/createAdminService');
const updateAdminService = require('./services/updateAdminService');
const destroyAdminService = require('./services/destroyAdminService');

const create = async (req, res) => {
  const data = req.body;

  const user = await createAdminService(data);

  return res.json(user);
};

const update = async (req, res) => {
  const { id } = req.user;
  const data = req.body;

  const user = await updateAdminService(id, data);

  return res.json(user);
};

const destroy = async (req, res) => {
  const { id } = req.user;

  await destroyAdminService(id);

  return res.status(204).json();
};

module.exports = {
  create,
  update,
  destroy,
};
