const { Admin } = require('../../models');

module.exports = {
  create: data => Admin.create(data),
  findByEmail: email => Admin.findOne({ where: { email } }),
  findById: id => Admin.findByPk(id),
  update: admin => admin.save(),
};
