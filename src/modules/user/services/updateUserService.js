const userRepository = require('../userRepository');
const AppError = require('../../../middlewares/AppError');

const updateUserService = async (id, data) => {
  const user = await userRepository.findById(id);

  if (!user || user.is_deleted) {
    throw new AppError('User not found', 404);
  }

  if (data.email) {
    const emailExists = await userRepository.findByEmail(data.email);

    if (emailExists) {
      throw new AppError('Email already registered');
    }
  }

  Object.assign(user, data);

  const newUser = await userRepository.update(user);

  return newUser;
};

module.exports = updateUserService;
