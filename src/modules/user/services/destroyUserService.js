const userRepository = require('../userRepository');
const AppError = require('../../../middlewares/AppError');

const destroyUserService = async id => {
  const user = await userRepository.findById(id);

  if (!user || user.is_deleted) {
    throw new AppError('User not found', 404);
  }

  user.is_deleted = true;

  await userRepository.update(user);
};

module.exports = destroyUserService;
