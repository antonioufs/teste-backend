const userRepository = require('../userRepository');
const AppError = require('../../../middlewares/AppError');

const createUserService = async data => {
  const user = await userRepository.findByEmail(data.email);

  if (user) {
    throw new AppError('Email already registered');
  }

  const newUser = await userRepository.create(data);

  return newUser;
};

module.exports = createUserService;
