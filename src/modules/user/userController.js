const createUserService = require('./services/createUserService');
const updateUserService = require('./services/updateUserService');
const destroyUserService = require('./services/destroyUserService');

const create = async (req, res) => {
  const data = req.body;

  const user = await createUserService(data);

  return res.json(user);
};

const update = async (req, res) => {
  const { id } = req.user;
  const data = req.body;

  const user = await updateUserService(id, data);

  return res.json(user);
};

const destroy = async (req, res) => {
  const { id } = req.user;

  await destroyUserService(id);

  return res.status(204).json();
};

module.exports = {
  create,
  update,
  destroy,
};
