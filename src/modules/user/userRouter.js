const router = require('express').Router();
const userController = require('./userController');
const ensureAuthenticated = require('../../middlewares/ensureAuthenticated');

router.post('/', userController.create);
router.put('/', ensureAuthenticated, userController.update);
router.delete('/', ensureAuthenticated, userController.destroy);

module.exports = router;
