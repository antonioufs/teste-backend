const { Actor } = require('../../models');

module.exports = {
  create: data => Actor.bulkCreate(data),
  list: actors => Actor.findAll({ where: { name: actors } }),
};
