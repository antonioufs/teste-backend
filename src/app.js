require('dotenv/config');
require('express-async-errors');
const express = require('express');
const routes = require('./modules/routes');
const AppError = require('./middlewares/AppError');

const app = express();

app.use(express.json());
app.use(routes);

app.use((err, request, response, next) => {
  if (err instanceof AppError) {
    return response.status(err.statusCode).json({
      status: 'error',
      message: err.message,
    });
  }

  console.log(err);

  return response.status(500).json({
    status: 'error',
    message: 'Internal server error',
  });
});

module.exports = app;
