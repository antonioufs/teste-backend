const database = require('../../models');

const clearDatabase = () => {
  return Promise.all(
    Object.keys(database.sequelize.models).map(key => {
      return database.sequelize.models[key].truncate({
        force: true,
      });
    }),
  );
};

module.exports = clearDatabase;
