const request = require('supertest');
const app = require('../../app');

const clearDatabase = require('../utils/clearDatabase');

describe('User', () => {
  beforeEach(async () => {
    await clearDatabase();
  });

  it('should be able to create a new user', async () => {
    const response = await request(app).post('/user').send({
      name: 'Antonio',
      email: 'user@hotmail.com',
      password: '123456',
    });

    expect(response.body).toHaveProperty('id');
  });

  it('should not be able to create a user with one e-mail thats already registered', async () => {
    const user = await request(app).post('/user').send({
      name: 'Antonio',
      email: 'user@hotmail.com',
      password: '123456',
    });

    expect(user.body).toEqual(
      expect.objectContaining({
        name: 'Antonio',
        email: 'user@hotmail.com',
      }),
    );

    const response = await request(app).post('/user').send({
      name: 'Antonio',
      email: 'user@hotmail.com',
      password: '123456',
    });

    expect(response.status).toBe(400);
  });

  it('should be able to update a user', async () => {
    const user = await request(app).post('/user').send({
      name: 'Antonio',
      email: 'user@hotmail.com',
      password: '123456',
    });

    expect(user.body).toEqual(
      expect.objectContaining({
        name: 'Antonio',
        email: 'user@hotmail.com',
      }),
    );

    const userLogin = await request(app).post('/session').send({
      email: 'user@hotmail.com',
      password: '123456',
      type: 'user',
    });

    const { token } = userLogin.body;

    const response = await request(app)
      .put('/user')
      .set('Authorization', `Bearer ${token}`)
      .send({
        name: 'Bispo',
      });

    expect(response.body).toEqual(
      expect.objectContaining({
        name: 'Bispo',
        email: 'user@hotmail.com',
      }),
    );
  });

  it('should be not able to update a user with one e-mail thats already registered', async () => {
    const user = await request(app).post('/user').send({
      name: 'Antonio',
      email: 'user@hotmail.com',
      password: '123456',
    });

    const user2 = await request(app).post('/user').send({
      name: 'Bispo',
      email: 'user2@hotmail.com',
      password: '123456',
    });

    expect(user.body).toHaveProperty('id');
    expect(user2.body).toHaveProperty('id');

    const userLogin = await request(app).post('/session').send({
      email: 'user@hotmail.com',
      password: '123456',
      type: 'user',
    });

    const { token } = userLogin.body;

    const response = await request(app)
      .put('/user')
      .set('Authorization', `Bearer ${token}`)
      .send({
        email: 'user2@hotmail.com',
      });

    expect(response.status).toBe(400);
  });

  it('should be not able to update a user with deleted user', async () => {
    const user = await request(app).post('/user').send({
      name: 'Antonio',
      email: 'user@hotmail.com',
      password: '123456',
    });

    expect(user.body).toHaveProperty('id');

    const userLogin = await request(app).post('/session').send({
      email: 'user@hotmail.com',
      password: '123456',
      type: 'user',
    });

    const { token } = userLogin.body;

    const updatedUser = await request(app)
      .put('/user')
      .set('Authorization', `Bearer ${token}`)
      .send({
        is_deleted: true,
      });

    expect(updatedUser.body).toEqual(
      expect.objectContaining({
        is_deleted: true,
      }),
    );

    const response = await request(app)
      .put('/user')
      .set('Authorization', `Bearer ${token}`)
      .send({
        name: 'Jesus',
      });

    expect(response.status).toBe(404);
  });

  it('should be able to delete a user', async () => {
    const user = await request(app).post('/user').send({
      name: 'Antonio',
      email: 'user@hotmail.com',
      password: '123456',
    });

    expect(user.body).toHaveProperty('id');

    const userLogin = await request(app).post('/session').send({
      email: 'user@hotmail.com',
      password: '123456',
      type: 'user',
    });

    const { token } = userLogin.body;

    const response = await request(app)
      .delete('/user')
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(204);
  });

  it('should be able to delete a user with deleted user', async () => {
    const user = await request(app).post('/user').send({
      name: 'Antonio',
      email: 'user@hotmail.com',
      password: '123456',
    });

    expect(user.body).toHaveProperty('id');

    const userLogin = await request(app).post('/session').send({
      email: 'user@hotmail.com',
      password: '123456',
      type: 'user',
    });

    const { token } = userLogin.body;

    await request(app).delete('/user').set('Authorization', `Bearer ${token}`);

    const response = await request(app)
      .delete('/user')
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(404);
  });
});
