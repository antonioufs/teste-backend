const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password_hash: DataTypes.STRING,
      password: DataTypes.VIRTUAL,
      is_deleted: DataTypes.BOOLEAN,
    },
    {
      tableName: 'users',
    },
  );

  User.beforeSave(async user => {
    if (user.password) {
      user.password_hash = await bcrypt.hash(user.password, 8);
    }
    return user;
  });

  User.prototype.checkPassword = async function (password) {
    return bcrypt.compare(password, this.password_hash);
  };

  User.prototype.toJSON = function () {
    const values = { ...this.get() };

    delete values.password_hash;
    return values;
  };

  return User;
};
