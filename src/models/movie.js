module.exports = (sequelize, DataTypes) => {
  const Movie = sequelize.define(
    'Movie',
    {
      name: DataTypes.STRING,
      director: DataTypes.STRING,
      genre: DataTypes.STRING,
      votes: DataTypes.INTEGER,
      rating: DataTypes.DECIMAL,
      details: DataTypes.TEXT,
    },
    {
      tableName: 'movies',
    },
  );

  Movie.associate = function (models) {
    models.Movie.belongsToMany(models.Actor, {
      through: 'movies_actors',
    });
  };

  return Movie;
};
