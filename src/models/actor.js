module.exports = (sequelize, DataTypes) => {
  const Actor = sequelize.define(
    'Actor',
    {
      name: DataTypes.STRING,
    },
    {
      tableName: 'actors',
    },
  );

  Actor.associate = function (models) {
    models.Actor.belongsToMany(models.Movie, {
      through: 'movies_actors',
    });
  };

  return Actor;
};
