const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const Admin = sequelize.define(
    'Admin',
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password_hash: DataTypes.STRING,
      password: DataTypes.VIRTUAL,
      is_deleted: DataTypes.BOOLEAN,
    },
    {
      tableName: 'admins',
    },
  );

  Admin.beforeSave(async admin => {
    if (admin.password) {
      admin.password_hash = await bcrypt.hash(admin.password, 8);
    }
    return admin;
  });

  Admin.prototype.checkPassword = async function (password) {
    return bcrypt.compare(password, this.password_hash);
  };

  Admin.prototype.toJSON = function () {
    const values = { ...this.get() };

    delete values.password_hash;
    return values;
  };

  return Admin;
};
