module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('movies_actors', {
      actor_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      movie_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('movies_actors');
  },
};
