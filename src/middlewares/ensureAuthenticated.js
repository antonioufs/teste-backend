const { verify } = require('jsonwebtoken');
const AppError = require('./AppError');
const authConfig = require('../config/auth');

const ensureAuthenticated = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    throw new AppError('JWT token is missing', 404);
  }

  const [, token] = authHeader.split(' ');

  try {
    const decoded = verify(token, authConfig.jwt.secret);

    req.user = {
      id: decoded.id,
      admin: decoded.admin,
    };

    return next();
  } catch {
    throw new AppError('Invalid JWT token');
  }
};

module.exports = ensureAuthenticated;
