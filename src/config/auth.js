module.exports = {
  jwt: {
    secret: process.env.JWT_SECRET,
    expiresIn: '5d',
  },
};
