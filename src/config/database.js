require('dotenv').config();

module.exports = {
  dialect: 'postgres',
  host: '127.0.0.1',
  username: process.env.DB_USER_NAME || 'postgres',
  password: process.env.DB_PASSWORD || '1234',
  database: 'ioasys',
  port: process.env.DB_PORT || '5432',
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true,
  },
};
